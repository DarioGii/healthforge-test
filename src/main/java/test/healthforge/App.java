package test.healthforge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Dario Ghunney Ware on 23/06/2017.
 */
@SpringBootApplication
public class App {

    public static void main(String args[]) throws Exception {
        SpringApplication.run(App.class, args);
    }

}