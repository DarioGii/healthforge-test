package test.healthforge.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import test.healthforge.exception.PatientNotFoundException;
import test.healthforge.model.Patient;
import test.healthforge.model.Response;

import java.util.List;

/**
 * Created by Dario Ghunney Ware on 23/06/2017.
 */
@Service
public class PatientService {

    @Value("${healthforge.requestURL}")
    private String requestURL;

    @Autowired
    private RestTemplate restTemplate;

    public Patient getPatient(String patientId) {
        try {
            return restTemplate.getForObject(requestURL + "/{patientId}", Patient.class, patientId);
        } catch (HttpClientErrorException e) {
            throw new PatientNotFoundException("Patient " + patientId + " not found. Error: " + e.getMessage());
        }
    }

    public List<Patient> getPatients(String lastName, String firstName, String zipCode, String sort, Integer page, Integer size) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(requestURL);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        builder
                .queryParam("lastName", lastName)
                .queryParam("firstName", firstName)
                .queryParam("zipCode", zipCode)
                .queryParam("sort", sort)
                .queryParam("page", page)
                .queryParam("size", size);

        return restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                Response.class).getBody().getContent();
    }

}