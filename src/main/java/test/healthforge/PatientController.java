package test.healthforge;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import test.healthforge.model.Patient;
import test.healthforge.service.PatientService;

import java.util.List;

/**
 * Created by Dario Ghunney Ware on 23/06/2017.
 */
@RestController
@RequestMapping("/patient")
public class PatientController {

    private PatientService patientService;

    public PatientService getPatientService() {
        return patientService;
    }

    @Autowired
    public void setPatientService(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("/list")
    public @ResponseBody
    List<Patient> listPatients(@RequestParam(value = "lastName", required = false) String lastName,
                               @RequestParam(value = "firstName", required = false) String firstName,
                               @RequestParam(value = "zipCode", required = false) String zipCode,
                               @RequestParam(value = "sort", required = false) String sort,
                               @RequestParam(value = "page", required = false) Integer page,
                               @RequestParam(value = "size", required = false) Integer size) {
        return patientService.getPatients(lastName, firstName, zipCode, sort, page, size);
    }

    @GetMapping("/{patientId}")
    public @ResponseBody
    Patient searchPatients(@PathVariable("patientId") String patientId) {
        return patientService.getPatient(patientId);
    }

}