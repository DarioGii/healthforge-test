package test.healthforge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Dario Ghunney Ware on 23/06/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Organisation {

    private String name;
    private Identifier identifier;
    private Address address;
    private Telecom telecom;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Identifier identifier) {
        this.identifier = identifier;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Telecom getTelecom() {
        return telecom;
    }

    public void setTelecom(Telecom telecom) {
        this.telecom = telecom;
    }

}