package test.healthforge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Dario Ghunney Ware on 23/06/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Telecom {

    private String usage;
    private String value;
    private String codeSystem;

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCodeSystem() {
        return codeSystem;
    }

    public void setCodeSystem(String codeSystem) {
        this.codeSystem = codeSystem;
    }

}