package test.healthforge.model;

/**
 * Created by Dario Ghunney Ware on 23/06/2017.
 */
public enum Gender {

    M,
    F

}