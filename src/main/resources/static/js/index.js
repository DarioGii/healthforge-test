$(function() {
    $('.parallax').parallax();
    $('select').material_select();
    $('.modal').modal();
    $('#pagination').materializePagination({
        align: 'right',
        firstPage:  1,
        lastPage:  99,
        urlParameter: 'page',
        useUrlParameter: false,
        onClickCallback: function(requestedPage) {
            listPatientData(requestedPage, $('#order').val());
        }
    });

    listPatientData($('#pagination li.active').data('page'), $('#order').val());

    $('#refresh').click(function() {
        listPatientData($('#pagination li.active').data('page'), $('#order').val())
    });

    $('#search_bar').keypress(function(e) {
        if(e.which == 13) {
            getPatientData($(this).val());
        }
    });

    $('#order').change(function() {
        listPatientData($('#pagination li.active').data('page'), $(this).val());
    });
});

function listPatientData(pageNum, order) {
    $('#progress_bar').html('<div class="progress"><div class="indeterminate"></div></div>');

    $.get('/patient/list?page=' + pageNum + '&sort=' + order)
    .done(function(patientList) {
        var tableRows = '';

        for(var i in patientList) {
            tableRows += '<tr id="'+ patientList[i].identifiers[0].value + '" href="patient_modal"><td>' + patientList[i].lastName + '</td><td>' + patientList[i].firstName + '</td><td>' + patientList[i].dateOfBirth + '</td></tr>'
        }

        $('#progress_bar').empty();
        $("#error_text").empty();
        $('#patient_data').html(tableRows);
        displayModal();
    });
}

function getPatientData(patientId) {
    $.get('/patient/' + patientId)
    .done(function(patientData) {
        $("#error_text").empty();
        $('#patient_data').html('<tr id="'+ patientData.identifiers[0].value + '" href="patient_modal"><td>' + patientData.lastName + '</td><td>' + patientData.firstName + '</td><td>' + patientData.dateOfBirth + '</td></tr>');
        displayModal();
    })
    .fail(function(error) {
        if(error.status === 400) {
            $('#error_text').text('Invalid input');
        } else if(error.status === 404) {
            $('#error_text').text('Patient not found');
        } else if(error.status === 500) {
            $('#error_text').text('Server error');
        }
    });
}

function displayModal() {
    $('table').find('tr').click(function() {
        var patientId = $(this).attr('id');

        $.get('/patient/' + patientId)
        .done(function(patientData) {
            $('#modal_header').text(patientData.prefix + ' ' + patientData.lastName + ', ' + patientData.firstName);
            $('#modal_last_name').text(patientData.lastName);
            $('#modal_first_name').text(patientData.firstName);
            $('#modal_gender').text(patientData.gender);
            $('#modal_dob').text(patientData.dateOfBirth);
            $('#modal_dod').text(patientData.dateOfDeath);
            $('#modal_addresses').html(formatAddresses(patientData.addresses));
            $('#modal_gp').text(patientData.managingOrganisation.name);
            $('#patient_modal').modal('open');
        });
    })
}

function formatAddresses(addresses) {
    var addressList = '<ul class="collection">';

    for(var i in addresses) {
        addressList += '<li class="collection-item">'
        + addresses[i].line1 + ', '
        + addresses[i].line2 + ', '
        + addresses[i].line4 + ', '
        + addresses[i].country + ', '
        + addresses[i].zipCode
        + '</li>';
    }

    addressList += '</ul>';

    return addressList;
}