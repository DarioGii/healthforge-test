package test.healthforge.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import test.healthforge.config.Config;
import test.healthforge.exception.PatientNotFoundException;
import test.healthforge.model.Patient;
import test.healthforge.model.Response;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

/**
 * Created by Dario Ghunney Ware on 26/06/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Config.class, PatientService.class})
public class PatientServiceTest {

    @Autowired
    private PatientService patientService;

    @Test
    public void getPatient() throws Exception {
        Patient expected = given()
                .pathParam("patientId", "575-702-8745")
                .get("https://api.interview.healthforge.io:443/api/patient/{patientId}")
                .getBody()
                .as(Patient.class);

        Patient actual = patientService.getPatient("575-702-8745");

        assertEquals(expected.getFirstName(), actual.getFirstName());
    }

    @Test(expected = PatientNotFoundException.class)
    public void patientNotFound() throws Exception {
        Patient actual = patientService.getPatient("Rosie");
    }

    @Test
    public void getPatients() throws Exception {
        Response expected = given()
                .param("size", "15")
                .get("https://api.interview.healthforge.io:443/api/patient/")
                .then()
                .extract()
                .as(Response.class);

        List<Patient> actual = patientService.getPatients(null,
                null,
                null,
                null,
                null,
                15);

        assertEquals(expected.getSize(), actual.size());
    }

}