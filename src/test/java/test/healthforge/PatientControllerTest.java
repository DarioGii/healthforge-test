package test.healthforge;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import test.healthforge.model.Gender;
import test.healthforge.model.Patient;
import test.healthforge.service.PatientService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Dario Ghunney Ware on 24/06/2017.
 */
@RunWith(SpringRunner.class)
public class PatientControllerTest {

    @Mock
    private PatientService mockPatientService;

    @Mock
    private List<Patient> mockPatients;

    private Patient testPatient;

    private PatientController patientController;

    @Before
    public void setUp() throws Exception {
        testPatient = new Patient();
        patientController = new PatientController();

        testPatient.setFirstName("First");
        testPatient.setLastName("Last");
        testPatient.setActive(true);
        testPatient.setPrefix("Mr");
        testPatient.setGender(Gender.M);
        patientController.setPatientService(mockPatientService);
    }

    @Test
    public void searchPatients() throws Exception {
        when(patientController.searchPatients("123")).thenReturn(testPatient);

        Patient actualPatient = patientController.searchPatients("123");

        verify(mockPatientService).getPatient("123");

        assertEquals(testPatient.getFirstName(), actualPatient.getFirstName());
        assertEquals(testPatient.getLastName(), actualPatient.getLastName());
        assertEquals(testPatient.isActive(), actualPatient.isActive());
        assertEquals(testPatient.getGender(), actualPatient.getGender());
        assertEquals(testPatient.getPrefix(), actualPatient.getPrefix());
    }

    @Test
    public void listPatients() throws Exception {
        when(patientController.listPatients(
                null,
                null,
                null,
                null,
                null,
                100))
                .thenReturn(mockPatients);

        List<Patient> actualPatients = patientController.listPatients(
                null,
                null,
                null,
                null,
                null,
                100);

        verify(mockPatientService).getPatients(
                null,
                null,
                null,
                null,
                null,
                100
        );

        assertNotNull(actualPatients);
        assertEquals(mockPatients.size(), actualPatients.size());
    }

}